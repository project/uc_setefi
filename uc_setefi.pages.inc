<?php

/**
 * @file
 * setefi menu items.
 *
 */


function uc_setefi_complete($cart_id = 0) {
	_uc_setefi_response();
	
	if (!$_SESSION['do_complete']) {
		drupal_goto('cart');
		exit();
	} else {
		$order = uc_order_load($_SESSION['setefi_rif_operazione']);
		// Add a comment to let sales team know this came in through the site.
		uc_order_comment_save($order->order_id, 0, t('Order created through website.'), 'admin');
		$output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));
		
		$page = variable_get('uc_cart_checkout_complete_page', '');
		if (!empty($page)) drupal_goto($page);
		return $output;
	}
}

